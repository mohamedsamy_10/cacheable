
# Laravel Cacheable
Laravel eloquent model cache

## Installation

```php
composer require mosamy/cacheable
```

## Usage


```php

class Posts extends Model
{
  use \Mosamy\Cacheable\Cacheable;
}

// this will get records from database.
$posts = Posts::get();

// this will get records from cache.
$posts = Posts::getCached();

//in shortcut
$posts = getCached('posts');

```

> To search in cache result you may use this code

```php
$posts = Posts::getCached()->searchCached('keyword', ['name', 'description']);
```

> To set the default cache search attributes  use this
> function:

```php

class Posts extends Model
{
  use \Mosamy\Cacheable\Cacheable;

  public static function searchableAttributes(){
    return ['name', 'description'];
  }

}

$posts = Posts::getCached()->searchCached('keyword');
```

> If you need to cache model with a relation or custom query you should override this function


```php

class Posts extends Model
{
  use \Mosamy\Cacheable\Cacheable;

  public static function cache(){
    return self::with('category')->get();
  }

}

```

> By default, Cache name format is: **prefix_(connection name)_(table
> name)** .
> By default there is no prefix for the cache name.
> If you want to set a cache prefix you should override this method.

```php

class Posts extends Model
{
  use \Mosamy\Cacheable\Cacheable;

  public static function cache_prefix(){
    return 'cache_';
  }

}
```

> So the default name will be like "cache_mysql_posts"

> To delete cache you can use this method:

```php
Post::deleteCache();
```

> or simply clear all cache

    php artisan cachhe:clear
