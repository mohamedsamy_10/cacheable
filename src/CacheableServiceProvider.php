<?php

namespace Mosamy\Cacheable;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Collection;

class CacheableServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
      Collection::macro('searchCached', function ($keyword, $attributes = []) {
        return $this->when($keyword, function($item) use($keyword, $attributes){

          if(!$attributes && !$item->isEmpty())
          $attributes = $item->first()->searchableAttributes();

          return $item->when($attributes, function ($item) use($keyword, $attributes){
            return $item->filter(function ($item) use($keyword, $attributes){
              foreach ($attributes as $value) {
                if(false !== stripos($item->{$value}, $keyword))
                return true;
              }
            });
          });
        });
      });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
      //
    }
}
