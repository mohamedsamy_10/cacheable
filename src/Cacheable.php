<?php

namespace Mosamy\Cacheable;

trait Cacheable
{

  public static function bootCacheable(){
    static::saved(fn() => static::recache());
    static::deleted(fn() => static::deleteCached());
  }

  public static function cache_prefix(){
    return null;
  }

  public static function searchableAttributes(){
    return [];
  }

  final static function cache_key(){
    return self::cache_prefix().\DB::getDefaultConnection().'_'.with(new static)->getTable();
  }

  public static function cache(){
    return self::get();
  }

  final static function deleteCached(){
    cache()->forget(self::cache_key());
  }

  final static function recache(){
    self::deleteCached();
    self::getCached();
  }

  final static function getCached(){
    return cache()->rememberForever(self::cache_key(), fn() => self::cache());
  }

}


 ?>
