<?php

  if (!function_exists('getCached')) {

    function getCached($model){
      return ('\App\Models\\'.ucfirst($model))::getCached();
    }

  }

?>
